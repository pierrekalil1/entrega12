SELECT *
FROM enderecos;

SELECT *
FROM enderecos AS e
INNER JOIN usuarios AS us
ON us.endereco_id = e.id
ORDER BY us.endereco_id;


SELECT r.id, r.nome, u.id, u.nome, u.email, u.senha, u.endereco_id
FROM usuario_rede_sociais AS ur
JOIN redes_sociais AS r
    ON ur.rede_social_id = r.id
JOIN usuarios u
    ON ur.usuario_id = u.id;


SELECT 
    re.rede_social_id, r.nome, u.id, u.nome, u.email, u.senha, u.endereco_id, e.id, e.rua, e.pais, e.cidade
FROM usuarios u
JOIN usuario_rede_sociais re
    ON re.usuario_id = u.id
JOIN redes_sociais r
    ON r.id = re.rede_social_id
JOIN enderecos e
    ON e.id = u.endereco_id;


SELECT r.nome, u.nome, u.email, e.cidade
FROM usuario_rede_sociais ur
JOIN redes_sociais r
    ON r.id = ur.rede_social_id
JOIN usuarios u 
    ON ur.usuario_id = u.id
JOIN enderecos e
    ON e.id = u.endereco_id;


SELECT re.nome, u.nome, u.email, e.cidade
FROM usuario_rede_sociais ur
JOIN redes_sociais re
    ON re.id = ur.rede_social_id
JOIN usuarios u
    ON ur.usuario_id = u.id
JOIN enderecos e
    ON e.id = u.endereco_id
WHERE
    re.nome = 'Youtube';


SELECT re.nome, u.nome, u.email, e.cidade
FROM usuario_rede_sociais ur
JOIN redes_sociais re
    ON re.id = ur.rede_social_id
JOIN usuarios u
    ON ur.usuario_id = u.id
JOIN enderecos e
    ON e.id = u.endereco_id
WHERE
    re.nome = 'Instagram';


SELECT re.nome, u.nome, u.email, e.cidade
FROM usuario_rede_sociais ur
JOIN redes_sociais re
    ON re.id = ur.rede_social_id
JOIN usuarios u
    ON ur.usuario_id = u.id
JOIN enderecos e
    ON e.id = u.endereco_id
WHERE
    re.nome = 'Facebook';


SELECT re.nome, u.nome, u.email, e.cidade
FROM usuario_rede_sociais ur
JOIN redes_sociais re
    ON re.id = ur.rede_social_id
JOIN usuarios u
    ON ur.usuario_id = u.id
JOIN enderecos e
    ON e.id = u.endereco_id
WHERE
    re.nome = 'TikTok';


SELECT re.nome, u.nome, u.email, e.cidade
FROM usuario_rede_sociais ur
JOIN redes_sociais re
    ON re.id = ur.rede_social_id
JOIN usuarios u
    ON ur.usuario_id = u.id
JOIN enderecos e
    ON e.id = u.endereco_id
WHERE
    re.nome = 'Twitter';