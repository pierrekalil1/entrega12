
INSERT INTO enderecos
    (rua, pais, cidade)
VALUES ('Avenida Higienópolis', 'Brasil', 'Londrina'),
       ('Avenida Paulista', 'Brasil', 'São Paulo'),
       ('Rua Marcelino Champagnat', 'Brasil', 'Curitiba');

INSERT INTO usuarios
    (nome, email, senha, endereco_id)
VALUES ('Cauan', 'cauan@exemple.com', '1234', 
            (SELECT 
                e.id
            FROM 
                enderecos AS e
            WHERE
                e.rua LIKE '%Paul%' AND e.cidade = 'São Paulo') ),

       ('Chrystian', 'chrystian@exemple.com', '4321', 
            (SELECT 
                e.id
            FROM 
                enderecos AS e
            WHERE
                e.rua LIKE '%Marce%' AND e.cidade = 'Curitiba') ),

       ('Matheus', 'matheus@exemple.com', '3214', 
            (SELECT 
                e.id
            FROM 
                enderecos AS e
            WHERE
                e.rua LIKE '%Higie%' AND e.cidade = 'Londrina') );

INSERT INTO redes_sociais
    (nome)
    VALUES  ('Youtube'),
            ('Twitter'),
            ('Instagram'),
            ('Facebook'),
            ('TikTok');

INSERT INTO usuario_rede_sociais
    (usuario_id, rede_social_id)
    VALUES  ((SELECT 
                u.id
            FROM
                usuarios AS u
            WHERE u.nome = 'Cauan'), 

            (SELECT 
                r.id
            FROM
                redes_sociais r
            where 
                r.nome = 'Youtube')),




            ((SELECT 
                u.id
            FROM
                usuarios AS u
            WHERE u.nome = 'Chrystian'), 

            (SELECT 
                r.id
            FROM
                redes_sociais r
            where 
                r.nome = 'Youtube')),




            ((SELECT 
                u.id
            FROM
                usuarios AS u
            WHERE u.nome = 'Matheus'), 

            (SELECT 
                r.id
            FROM
                redes_sociais r
            where 
                r.nome = 'Youtube')),




            ((SELECT 
                u.id
            FROM
                usuarios AS u
            WHERE u.nome =  'Chrystian'), 

            (SELECT 
                r.id
            FROM
                redes_sociais r
            where 
                r.nome = 'Twitter')),




            ((SELECT 
                u.id
            FROM
                usuarios AS u
            WHERE u.nome = 'Cauan'), 

            (SELECT 
                r.id
            FROM
                redes_sociais r
            where 
                r.nome = 'Twitter')),




            ((SELECT 
                u.id
            FROM
                usuarios AS u
            WHERE u.nome = 'Matheus'), 

            (SELECT 
                r.id
            FROM
                redes_sociais r
            where 
                r.nome = 'Instagram')),




            ((SELECT 
                u.id
            FROM
                usuarios AS u
            WHERE u.nome = 'Matheus'), 

            (SELECT 
                r.id
            FROM
                redes_sociais r
            where 
                r.nome = 'Facebook')),




            ((SELECT 
                u.id
            FROM
                usuarios AS u
            WHERE u.nome = 'Chrystian'), 

            (SELECT 
                r.id
            FROM
                redes_sociais r
            where 
                r.nome = 'Instagram')),

            


            ((SELECT 
                u.id
            FROM
                usuarios AS u
            WHERE u.nome = 'Cauan'), 

            (SELECT 
                r.id
            FROM
                redes_sociais r
            where 
                r.nome = 'TikTok'));